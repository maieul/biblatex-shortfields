biblatex-shortfields
==================

Use short version of fields (series, journal) when defined.

Print a list of the short versions of the fields, mixing multiple fields type.
For example, a liste mixing short version of series field and short version of journal field. 
